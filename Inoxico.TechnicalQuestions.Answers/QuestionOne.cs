﻿
using System.Linq;
namespace Inoxico.TechnicalQuestions.Answers
{
    public class QuestionOne
    {
        public static int GetLongestSentance(string s)
        {
            if (s.Length < 1 || s.Length > 100)
            {
                throw new ArgumentOutOfRangeException(nameof(s), $"The length of String must be within the range 1 to 100" );
            }

            var sentences = s.Split('.', '!', '?');
            int mostSentenceWords = 0;

            foreach (string sentence in sentences)
            {
                var words = sentence.Trim().Split(' ');

                if (words.Length > mostSentenceWords)
                    mostSentenceWords = words.Length;
            }

            return mostSentenceWords;
        }
    }
}




